class Process_bar:
    """ Variable style status bar """
    VARIABLE_OPTION_BAR = {
        'default': {  # Step: 10/10 | Percent: 100/100 [██████████████████████████████████████████████████]
            'fill': '█',
            'prefix': '[',
            'suffix': ']',
            'empty_fill': ' '
        },
        'pointer': {  # Step: 10/10 | Percent: 100/100 |>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>|
            'fill': '>',
            'prefix': '|',
            'suffix': '|',
            'empty_fill': '-'
        }
    }

    def __init__(
            self,
            total,
            bar_length = 50,
            prefix = VARIABLE_OPTION_BAR['default']['prefix'],
            suffix = VARIABLE_OPTION_BAR['default']['suffix'],
            fill = VARIABLE_OPTION_BAR['default']['fill'],
            empty_fill = VARIABLE_OPTION_BAR['default']['empty_fill'],  # TODO: add to ReadMe
            option_bar_available = 'default'  # TODO: add to ReadMe
    ) -> None:
        assert total >= 1, f'Error of the entered total number of steps.\nYou entered: {total}'

        self.total = total
        self.bar_length = bar_length
        self.bar = []
        self.step = 1
        self.data = {}
        len_step = self.bar_length / total

        if option_bar_available == 'default':
            self.prefix = prefix
            self.suffix = suffix
            self.fill = fill
            self.empty_fill = empty_fill
        else:
            self.__choice_bar_style(option_bar_available)

        for i in range(total + 1):
            percent = round(((i * len_step) / self.bar_length) * 100)
            self.data[i] = ({
                'percent': percent,
                'value': round((self.bar_length * percent) / 100)
            })

        for i in range(self.bar_length):
            self.bar.append(self.empty_fill)

        self.__print_empty_bar()

    def update(self, legend = None) -> None:

        assert self.total >= 1, 'Total is 0'
        self.legend = legend

        for i in range(self.data[self.step]['value']):
            self.bar[i] = self.fill

        self.__print_bar()
        self.step = self.step + 1

    def __choice_bar_style(self, style: str) -> None:
        assert style in self.VARIABLE_OPTION_BAR, f'Style "{style}" not found'

        self.prefix = self.VARIABLE_OPTION_BAR[style]['prefix']
        self.suffix = self.VARIABLE_OPTION_BAR[style]['suffix']
        self.fill = self.VARIABLE_OPTION_BAR[style]['fill']
        self.empty_fill = self.VARIABLE_OPTION_BAR[style]['empty_fill']

    def __print_bar(self) -> None:
        if self.legend is None:
            legend = ''
        else:
            legend = f" Legend: {self.legend}"

        print(
                f"Step: {self.step}/{self.total} | "
                f"Percent: {self.data[self.step]['percent']}/100 "
                f"{self.prefix}{''.join(self.bar)}{self.suffix}"
                f"{legend}",
                end = '\r'
        )

    def __print_empty_bar(self) -> None:
        print(
                f"Step: 0/{self.total} | "
                f"Percent: 0/100"
                f"{self.prefix}{''.join(self.bar)}{self.suffix} | ",
                end = '\r'
        )
