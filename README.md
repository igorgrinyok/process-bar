# PY Process bar

Console process bar.

## Parameters

### processbar:

```
total - Total items to bar(required)
bar_length - Bar length(Default: 50)
prefix - Element at the beginning of the bar(Default: [)
suffix - Element at the end of the bar(Default: ])
fill - Bar fill(Default: █)
```

### bar.update:

```
legend - text displayed to the right of the bar(Default: None)
```

## Usage

```
import processbar

bar = processbar(total = 10) # 10 - total items to bar
 
### Your code
bar.update() # Next step in bar
```
