from setuptools import setup, find_packages

setup(
        name = 'process_bar',
        version = '0.1',
        license = 'MIT',
        author = "Igor",
        packages = find_packages('src'),
        package_dir = {'': 'src'},
        url = 'https://gitlab.com/igorgrinyok/process-bar'
)
